#!/usr/bin/env python3

# viz dokumentaci k api https://rapidoc.croapp.cz/

import requests
import logging
import io
import os

import mutagen
from mutagen.mp3 import MP3, EasyMP3
from mutagen.easyid3 import EasyID3
from mutagen.id3 import ID3
from mutagen.apev2 import APEv2, APEv2File, APENoHeaderError

class SessionL(requests.Session): # requests Session s automatickým logováním GET požadavků
    def get_logged(self, url, *aargs, **kwargs):
        logging.info("GET " + url)
        try:
            r = self.get(url, *aargs, **kwargs)
        except requests.exceptions.RequestException as e:
            logging.warn('Chyba spojení, zkouším znova...')
            print(aargs)
            time.sleep(prodleva*5)
            r = self.get(url, *aargs, **kwargs)
        logging.info(r.status_code)
        return r
    
    def vyžer_vše(self, url, *aargs, **kwargs):
        r = self.get_logged(url, *aargs, **kwargs)
        rj =  r.json()
        data = rj['data']
        meta = rj['meta']
        try:
            while rj['links']['next'] is not None:
                r = self.get_logged(rj['links']['next'], *aargs, **kwargs)
                rj = r.json()
                data.extend(rj['data'])
        except KeyError:
            pass
        return {'meta': meta, 'data': data}

headers={'user-agent':'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0',
        'accept-language':'cs',
        'cache-control':'max-age=0',
        'dnt':'1',
        'upgrade-insecure-requests':'1',
        'accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        }

s = SessionL()
s.headers.update(headers)
s.verify=False # kašlem na ověření TSL certifikátu
requests.urllib3.disable_warnings() # fujka

# Zde je natvrdo link na dětské pořady, ostatní možnosti viz na adrese https://api.mujrozhlas.cz/episodes/
episody = s.vyžer_vše("https://api.mujrozhlas.cz/topics/8ed08518-9d92-437c-a96f-e5df046aff4e/episodes")

# složky napevno napraseny
prefix = '.'
dir_origfilename = os.path.join(prefix, 'by_origfilename')
#dir_ep_uuid = os.path.join(prefix, 'by_episode_id')
dir_human = os.path.join(prefix, 'pořady')
dir_meta = os.path.join(prefix, 'meta')
os.makedirs(dir_origfilename, exist_ok = True)
#os.makedirs(dir_ep_uuid, exist_ok = True)
os.makedirs(dir_human, exist_ok = True)
os.makedirs(dir_meta, exist_ok = True)

for ep in episody['data']:
    al = ep['attributes']['audioLinks']
    if len(al) > 1:
        logging.warn("Nalezeno víc audio odkazů, používám první" + str(al))
    al_url = al[0]['url']
    orig_filename = al_url.split('/')[-1]
    dlpath = os.path.join(dir_origfilename, orig_filename)
    metapath = os.path.join(dir_meta, orig_filename + '.json')
    if os.path.exists(metapath): 
        logging.info(metapath + " existuje, už zjevně staženo")
        continue
    r = s.get_logged(al_url)
    if r.status_code != 200:
        logging.warn("Divná odpověď %d, ignoruji a jedu dál (%s)" % (r.status_code, al_url))
        continue
    with open(dlpath, 'wb') as fil:
        fil.write(r.content)
    id3 = EasyMP3(dlpath)
    id3tags = dict(id3)
    id3.clear()
    id3.save(dlpath)
    try:
        ape = APEv2File(dlpath)
    except APENoHeaderError:
        ape = APEv2File()
    title = ep['attributes']['shortTitle']
    ape['Title'] = title
    part = ep['attributes']['part']
    ape['Track'] = str(part)
    try:
        ape['TotalParts'] = str(ep['attributes']['mirroredSerial']['totalParts'])
    except KeyError:
        pass
    show = ep['attributes']['mirroredShow']['title']
    ape['Album'] = show
    ape['ShowUrl'] = ep['relationships']['show']['links']['related']
    ape['EpisodeId'] = ep['id']
    ape['File'] = al_url
    for key, val in id3tags.items():
        ape['id3_'+key] = val
    ape.save(dlpath)
    targetdir = os.path.join(dir_human, show, title) if part is not None else(
                os.path.join(dir_human, show) )
    os.makedirs(targetdir, exist_ok=True)
    targetpath = os.path.join(targetdir, '%s_%d.mp3'%(title, part) if part is not None else '%s.mp3'%(title,))
    os.link(dlpath, targetpath)
    # Nakonec si uložíme metadat (čímž taky budeme vědět, že jsme si danou episodu už stáhli)
    requests.compat.json.dump(ep, open(metapath, 'w'), indent='  ')
    
    
